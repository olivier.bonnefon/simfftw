

# include <stdlib.h>
# include <stdio.h>
# include <time.h>
#include <iostream>
using namespace std;
# include <fftw3.h>
# include <math.h>
#include <string.h>
#include "visuTiff.h"

#define NGEN 160
double kernelChi[2*NGEN];
double etaXY[2*NGEN];
double etaXYc[2*NGEN];
fftw_complex *fftwKernelChi;


void initChi(){
  int i=0;
  double scale=1.;
  double * chi=kernelChi;
  for (i=0;i<NGEN;i++){
    *chi++=10.0/NGEN+5.0*((1.0*(NGEN-i))/NGEN)*1.0/NGEN;
  }
  for (i=NGEN;i<2*NGEN;i++)
      *chi++=0;
      
    //*chi++=scale*(0.5*i)/NGEN;
  
}
void buildOutChi(){
  int i;
  fftw_plan plan_forward;
  initChi();
  plan_forward = fftw_plan_dft_r2c_1d ( 2*NGEN,  kernelChi, fftwKernelChi, FFTW_ESTIMATE );
  
  fftw_execute ( plan_forward );
  for (i=0;i<NGEN+1;i++){
    fftwKernelChi[i][0]*=1.0/(2*NGEN);
    fftwKernelChi[i][1]*=1.0/(2*NGEN);
  }
  fftw_destroy_plan(plan_forward);
}

void export1D(const char * name, double *val){
  FILE* fout=fopen(name,"w");
  int i;
  
  for (i=0;i<2*NGEN;i++){
    fprintf(fout,"%i  %e\n",i,*val++);
  }
}

void zero(double *z,int n){
  int j;
  for(j=0;j<n;j++){
    *z=0;
    z++;
  }
}
/*effectue le calul de la convolution
 *out=in*kernel
 *
 */
void convol1D(double *in,double *out,double *buf,fftw_complex *kernel,fftw_complex *buffft1,fftw_complex *buffft2){
  int i,j;
  fftw_plan plan_1;
  plan_1 = fftw_plan_dft_r2c_1d ( 2*NGEN, in, buffft1, FFTW_ESTIMATE );
  fftw_execute ( plan_1);
  fftw_destroy_plan(plan_1);
  for (i=0;i<(NGEN+1);i++){
    buffft2[i][0]=(buffft1[i][0]*kernel[i][0]-buffft1[i][1]*kernel[i][1]);
    buffft2[i][1]=(buffft1[i][0]*kernel[i][1]+buffft1[i][1]*kernel[i][0]);
  }
  fftw_plan plan_backward = fftw_plan_dft_c2r_1d ( 2*NGEN, buffft2, out, FFTW_ESTIMATE );

  fftw_execute ( plan_backward );
  fftw_destroy_plan(plan_backward);
  memcpy(out,out+NGEN,NGEN*sizeof(double));
  zero(out+NGEN,NGEN);
 
}





int main(){
  int i=0;
  double * buf=(double *) malloc(sizeof ( double ) * 2*NGEN);
  fftwKernelChi = ( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * (NGEN+1) );
  fftw_complex *buf1 = ( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * (NGEN+1) );
  fftw_complex *buf2 = ( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * (NGEN+1) );
 
  buildOutChi();
  export1D("kernelChi",kernelChi);
  zero(etaXY,2*NGEN);
  //for (i=0;i<NGEN;i++){
  for (i=0;i<NGEN;i++)
    etaXY[i]=1;
  convol1D(etaXY,etaXYc,NULL,fftwKernelChi,buf1,buf2);
  export1D("etaxy",etaXY);
  export1D("etaxyc",etaXYc);
    
}
