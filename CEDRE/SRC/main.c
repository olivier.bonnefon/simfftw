

# include <stdlib.h>
# include <stdio.h>
# include <time.h>
#include <iostream>
using namespace std;
# include <fftw3.h>
# include <math.h>
#include <string.h>
#include "visuTiff.h"
int nx;
int ny;
int nyh;
double dx;
double dy;

double *inKernel;

fftw_complex *outKernel;
#define USE_1D_CONVOL
#define NGEN 10
int curGen=0;
double kernelChi[2*NGEN];
fftw_complex *outEta=NULL;
fftw_complex *outRes=NULL;

//#define CEDRE_DEBUG

//export la val(x,y) pour une visualisation avec gnuplot:
//gnuplot
//>splot("name") u 1:2:3
//
void export3D(const char * name, double *val){
  FILE* fout=fopen(name,"w");
  int i,j;
  double curX=0.5*dx;
  double curY=0.5*dy;
  
  double *pv=val;
  for (i=0;2*i<ny;i++){
    pv=val+i*nx;
    curX=0.5*dx;
    for (j=0;2*j<nx;j++){
      fprintf(fout,"%e %e %e\n",curX,curY,*pv);
      pv++;
      curX+=dx;
    }
    curY+=dy;
  }
  fclose(fout);
}
void export1D(const char * name, double *val){
  FILE* fout=fopen(name,"w");
  int i;
  
  for (i=0;i<2*NGEN;i++){
    fprintf(fout,"%i  %e\n",i,*val++);
  }
}
/*
 *definition du noyau centre en (0,0) en vue de la transformation de fourier
 *
 */
void evalKernel(){
  int i,j;
  double xmiddle=(nx*dx)/2.0;
  double ymiddle=(ny*dy)/2.0;
  double curX=0.5*dx;
  double curY=0.5*dy;
  
  double mass=0;
  
  for (j=0;j<ny;j++){
    curX=0.5*dx;
    for (i=0;i<nx;i++){
      double dd2=(curX-xmiddle)*(curX-xmiddle)+(curY-ymiddle)*(curY-ymiddle);
      inKernel[i+j*nx]=exp(-0.1*dd2);
      mass+=inKernel[i+j*nx]*dx*dy;
      curX+=dx;
    }
    curY+=dy;
  }
  double *pv=inKernel;
  for (i=0;i<nx*ny;i++){
    *pv=(*pv)/mass;
    pv++;
  }
  export3D("kernel.txt",inKernel);
}
/*
 *Transformation de fourier du noyau afin de calculer les convolutions
 *
 */
void buildOutKernel(){
  int i;
  fftw_plan plan_forward;
  evalKernel();
  plan_forward = fftw_plan_dft_r2c_2d ( nx, ny, inKernel, outKernel, FFTW_ESTIMATE );
  
  fftw_execute ( plan_forward );
  for (i=0;i<nx*nyh;i++){
    outKernel[i][0]*=(dx*dy)/(nx*ny);
    outKernel[i][1]*=(dx*dy)/(nx*ny);
  }
  fftw_destroy_plan(plan_forward);
}
/*
 * Definition de l'etat initial
 *
 */
void initEta(double *eta){
  int i,j;
  for (i=0;i<ny;i++)
    for (j=0;j<nx;j++)
      eta[j+i*nx]=0;

  for (i=0.1*0.5*ny;i<0.5*0.5*ny;i++)
    for (j=0.25*0.5*nx;j<0.5*0.5*nx;j++)
      eta[j+i*nx]=1;

}

double computeMass(double *pv){
  int i;
  double res=0;
  for (i=0;i<nx*ny;i++)
    res+=pv[i]*dx*dy;
  return res;
}
void forceZeroAfterConvol(double *val){
  for (int j=0;j<nx;j++)
    for (int i=0;i<ny;i++){
      if (j>nx/2 && i>ny/2)
        *val=0;
      val++;
    }
}
/*effectue le calul de la convolution
 *out=in*kernel
 *
 */
void convol(double *in,double *out,double *buf,fftw_complex *kernel,fftw_complex *buffft1,fftw_complex *buffft2){
  int i,j;
  fftw_plan plan_1;
  plan_1 = fftw_plan_dft_r2c_2d ( nx, ny, in, buffft1, FFTW_ESTIMATE );
  fftw_execute ( plan_1);
  fftw_destroy_plan(plan_1);
  for (i=0;i<nx * nyh;i++){
    buffft2[i][0]=(buffft1[i][0]*kernel[i][0]-buffft1[i][1]*kernel[i][1]);
    buffft2[i][1]=(buffft1[i][0]*kernel[i][1]+buffft1[i][1]*kernel[i][0]);
  }
  fftw_plan plan_backward = fftw_plan_dft_c2r_2d ( nx, ny, buffft2, out, FFTW_ESTIMATE );

  fftw_execute ( plan_backward );
  fftw_destroy_plan(plan_backward);
  int nx2=nx/2;
  int ny2=ny/2;
  
  for (j=0;j<ny2;j++){
    double *pLinHaut=out + (j*nx);
    double *pLinBas=out + ((j+ny2)*nx);
    size_t scp=nx2*sizeof(double);
    memcpy(buf,pLinBas+nx2,scp);
    memcpy(pLinBas+nx2,pLinHaut,scp);
    memcpy(pLinHaut,buf,scp);

    memcpy(buf,pLinHaut+nx2,scp);
    memcpy(pLinHaut+nx2,pLinBas,scp);
    memcpy(pLinBas,buf,scp);
  }
  forceZeroAfterConvol(out);
}

/*
 *taux de reproduction
 *
 *
 */
void initGamma(double *g){
  int i=0;
  for (i=0;i<NGEN;i++){
    if (i<40)
      *g=0;
    else{
      *g=0.1;
    }
    g++;
  }
}
/*
 *initialisation du noyau de compétition
 *
 */
void initChi(){
  int i=0;
  double scale=1.;
  double * chi=kernelChi;
  for (i=0;i<NGEN;i++){
    *chi++=10.0/NGEN+5.0*((1.0*(NGEN-i))/NGEN)*1.0/NGEN;
  }
  for (i=NGEN;i<2*NGEN;i++)
      *chi++=0;      
    //*chi++=scale*(0.5*i)/NGEN;
  
}

void zero(double *z,int n){
  int j;
  for(j=0;j<n;j++){
    *z=0;
    z++;
  }
}
/*
 *Affichage d'un resume de la fonction eta(x,y)
 *
 */
void printState(double *eta){
  int i,j;
  double min=*eta;
  double max=*eta;
  double card=0;
  int nnullPixel=0;
  for (i=0;i<ny;i++){
    for (j=0;j<nx;j++){
      double val=*eta++;//[a*nx*ny+i*nx+j]
      card+= dx*dy*val;
      if (val) nnullPixel++;
      if (val<min)
        min=val;
      if (val>max)
        max=val;
    }
  }
  cout<<min<<" "<<max<<" "<<card<<" "<<nnullPixel<<endl;
  
}
/*
 *Affichage d'un resume de l'etat du systeme eta_numGen(x,y)
 *
 */
void printStates(double *eta){
  int a,ap;
  cout<<"BEGIN PRINT STATE"<<endl;
  cout<<"min max Npop NpixelnonNull"<<endl;
  for (a=0;a<NGEN;a++){
    cout<<"     print genaration "<<a<<" ";
    printState(eta);
    eta=eta+nx*ny;
  }
  cout<<"END PRINT STATE"<<endl;
}
#ifdef USE_1D_CONVOL

double etaXY[2*NGEN];
double etaXYc[2*NGEN];
fftw_complex *fftwKernelChi;

void convol1D(double *in,double *out,double *buf,fftw_complex *kernel,fftw_complex *buffft1,fftw_complex *buffft2){
  int i,j;
  fftw_plan plan_1;
  plan_1 = fftw_plan_dft_r2c_1d ( 2*NGEN, in, buffft1, FFTW_ESTIMATE );
  fftw_execute ( plan_1);
  fftw_destroy_plan(plan_1);
  for (i=0;i<(NGEN+1);i++){
    buffft2[i][0]=(buffft1[i][0]*kernel[i][0]-buffft1[i][1]*kernel[i][1]);
    buffft2[i][1]=(buffft1[i][0]*kernel[i][1]+buffft1[i][1]*kernel[i][0]);
  }
  fftw_plan plan_backward = fftw_plan_dft_c2r_1d ( 2*NGEN, buffft2, out, FFTW_ESTIMATE );

  fftw_execute ( plan_backward );
  fftw_destroy_plan(plan_backward);
  memcpy(out,out+NGEN,NGEN*sizeof(double));
  zero(out+NGEN,NGEN);
  //printf("convol1d done\n");
 
}
void buildOutChi(){
  int i;
  fftw_plan plan_forward;
  //initChi();
  fftwKernelChi=( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * (NGEN+1) );
  plan_forward = fftw_plan_dft_r2c_1d ( 2*NGEN,  kernelChi, fftwKernelChi, FFTW_ESTIMATE );
  
  fftw_execute ( plan_forward );
  for (i=0;i<NGEN+1;i++){
    fftwKernelChi[i][0]*=1.0/(2*NGEN);
    fftwKernelChi[i][1]*=1.0/(2*NGEN);
  }
  fftw_destroy_plan(plan_forward);
}
void computeCompetition(double * eta, double * compet, double *chi){
  int i,j,a;
  for (i=0;2*i<ny;i++){
    for (j=0;2*j<nx;j++){
      zero(etaXY,2*NGEN);
      for (a=0;a<curGen;a++){
        etaXY[a]=eta[a*nx*ny+i*nx+j];
      }
      convol1D(etaXY,etaXYc,NULL,fftwKernelChi,outEta,outRes);
      for (a=0;a<curGen;a++){
        compet[a*nx*ny+i*nx+j]=etaXYc[a];
      }
      //covoler etaXY
      //remplir compet
    }
  }
}
#else
/*
 *calcul de la competition exerce par eta sur l'espacexgeneration
 *input : eta etat de la pop
 *output : competition K(x,y,a)
 */
void computeCompetition(double * eta,double * compet,double *chi){
  int i,j,a,ap=0;
  zero(compet,NGEN*nx*ny);
  for (a=0;a<curGen;a++){
    double *pEta=eta+nx*ny*a;
    for (ap=a;ap<curGen;ap++){
      double chiaap=chi[NGEN-1+a-ap];
      double *pCompet=compet+(nx*ny)*a;
      for (i=0;i<ny;i++){
        for (j=0;j<nx;j++){
          //compet[i*nx+j+(nx*ny)*a]+=chiaap*eta[ap*nx*ny+i*nx+j];
          *(pCompet++)=chiaap*(*pEta++);
          }
      }
    }
  }
}
#endif

/*
 *calcul de la reproduction
 *input: eta etat de la pop
 *input: gamma coefs de reporduction
 *input: kernel le noyau
 *output: RR(x,y) la reproduction
 */
void computeR(double * eta, double *gamma,double *RR,double *buf,double *bufConvol,fftw_complex *kernel,fftw_complex *buffft1,fftw_complex *buffft2){
  int ij;
  int a;
  zero(buf,nx*ny);
  double *pPop=eta+39*nx*ny;
#ifdef CEDRE_DEBUG_COMPUTER
   static int cmp=0;
#endif
  for (a=39;a<NGEN;a++){
    for(ij=0;ij<nx*ny;ij++)
      buf[ij]+=gamma[a]*(*pPop++);
  }
  convol(buf,RR,bufConvol,kernel,buffft1,buffft2);
#ifdef CEDRE_DEBUG_COMPUTER
  char fname[256];
  sprintf(fname,"RR%i",cmp);
  export3D(fname,RR);
  cmp++;
#endif
}
int main(){
  //spatial discretisation
  nx=2*100;
  ny=2*200;
  int ncells=(nx/2)*(ny/2);
  nyh = ( ny / 2 ) + 1;
  dx=0.1;
  dy=0.1;
  nyh = ( ny / 2 ) + 1;
  visuTiff_Init(NGEN,nx/2,ny/2);
  //les NGEN classes d'ages:
  //la population d'age a de la ligne i et de la colonne j est eta[nx*ny*a + j+i*nx]
  double * eta=(double *) calloc(nx * ny * NGEN,sizeof ( double ) ); 
  double * etac=(double *) malloc(sizeof ( double ) * nx * ny );
  double * buf=(double *) malloc(sizeof ( double ) * nx * ny);
  double * gamma=(double *) malloc(sizeof ( double ) * NGEN );
//  double * kernelChi=(double *) malloc(sizeof ( double ) * NGEN *2);
  double * compet=(double *) calloc(nx * ny *NGEN,sizeof ( double ) );
  double * RR=(double *) malloc(sizeof ( double ) * nx * ny );
  
  //le noyau
  int i,j;
  inKernel = ( double * ) malloc ( sizeof ( double ) * nx * ny );
  outKernel = ( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * nx * nyh );
  outEta = ( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * nx * nyh );
  outRes = ( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * nx * nyh );
  buildOutKernel();
  initChi();
#ifdef USE_1D_CONVOL
  buildOutChi();
#endif
  initGamma(gamma);
  initEta(eta);
  
#ifdef CEDRE_DEBUG
  export3D("eta0",eta);
  convol(eta,etac,buf,outKernel,outEta,outRes);
  export3D("eta0c",etac);
#endif
  int nStepByYear=1;
  int numStep=0;
  double dt=1.0/nStepByYear;
  for (curGen=0;curGen<NGEN;curGen++){
#ifdef CEDRE_DEBUG
    cout<<" curGen= "<<curGen;
    printStates(eta);
#endif
    visuTiff_Register(eta,curGen);
    for (numStep=0;numStep<nStepByYear;numStep++){
      // la competition
      computeCompetition(eta,compet,kernelChi);
      computeR(eta,gamma,RR,buf,etac,outKernel,outEta,outRes);
      double * pPop;
      double * pComp;
      for (int igen=0;igen<curGen;igen++){
        pPop=eta +nx*ny*igen;
        pComp=compet +nx*ny*igen;
        for (i=0;i<ncells;i++){
          *pPop=(*pPop)/(1+dt*(*pComp));
          pPop++;
          pComp++;
        }
      }
      // le reproduction
      computeR(eta,gamma,RR,buf,etac,outKernel,outEta,outRes);
#ifdef CEDRE_DEBUG
      cout<<" RR ";
      printState(RR);
      cout<<endl;
#endif
      pPop=eta;
      double *pR=RR;
      for (i=0;i<ncells;i++)
        (*pPop++)+=dt*(*pR++);
    }
    export3D("etastep1",eta);
    //on fait veillir la population
    if (curGen != NGEN-1){
      memmove(eta+nx*ny,eta,(curGen+1)*nx*ny*sizeof(double));
      zero(eta,nx*ny);
    }
    printf("generation %i done\n",curGen);
  }
  visuTiff_End();

  free(eta);
  free(etac);
  free(buf);
  free(gamma);
  free(compet);
  free(RR);
  fftw_free(outEta);
  fftw_free(outRes);
}
