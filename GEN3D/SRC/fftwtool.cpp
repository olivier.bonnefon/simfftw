#include <fftw3.h>
#include <string.h>
#include "fftwtool.h"
#include <math.h>
int nx;
int ny;
int nz;
int nzh;
double dx;
double dy;
double dz;
double *inKernel;

fftw_complex *outKernel;

//double kernelChi[2*NGEN];
fftw_complex *outEta=NULL;
fftw_complex *outRes=NULL;

//#define CEDRE_DEBUG

void saveVTKStep(const char * name, double *val,int sx,int sy,int sz){
  FILE* fout=fopen(name,"w");
  int i,j,k;
  double curX=0.5*dx;
  double curY=0.5*dy;
  double curZ=0.5*dz;  
  double *pv=val;
  fprintf(fout,"# vtk DataFile Version 2.0\nCube structuré VTK\nASCII\n\nDATASET STRUCTURED_POINTS\nDIMENSIONS %d %d %d\nORIGIN -1 -1 -1\nSPACING 1 1 1\n\nPOINT_DATA %i\nSCALARS ma_variable double 1\nLOOKUP_TABLE default\n",
          nx/sx,ny/sy,nz/sz,nx*ny*nz/(sx*sy*sz));
  for (k=0;k<nz;k+=sz){
    for (i=0;i<ny;i+=sy){
      for (j=0;j<nx;j+=sx){
        fprintf(fout,"%e ",val[j+i*nx+k*nx*ny]);
        pv++;
      }
      fprintf(fout,"\n");
    }
  }
  fclose(fout);

}


void saveVTK(const char * name, double *val){
  FILE* fout=fopen(name,"w");
  int i,j,k;
  double curX=0.5*dx;
  double curY=0.5*dy;
  double curZ=0.5*dz;  
  double *pv=val;
  fprintf(fout,"# vtk DataFile Version 2.0\nCube structuré VTK\nASCII\n\nDATASET STRUCTURED_POINTS\nDIMENSIONS %d %d %d\nORIGIN -1 -1 -1\nSPACING 1 1 1\n\nPOINT_DATA %i\nSCALARS ma_variable double 1\nLOOKUP_TABLE default\n",
          nx,ny,nz,nx*ny*nz);
  for (k=0;k<nz;k++){
    for (i=0;i<ny;i++){
      for (j=0;j<nx;j++){
        fprintf(fout,"%e ",*pv);
        pv++;
      }
      fprintf(fout,"\n");
    }
  }
  fclose(fout);

}
//export la val(x,y) pour une visualisation avec gnuplot:
//gnuplot
//>splot("name") u 1:2:3
//
void export3D(const char * name, double *val){
  FILE* fout=fopen(name,"w");
  int i,j,k;
  double curX=0.5*dx;
  double curY=0.5*dy;
  double curZ=0.5*dz;  
  double *pv=val;
  for (k=0;2*k<nz;k++){
    for (i=0;2*i<ny;i++){
      pv=val+i*nx+k*nx*ny;
      curX=0.5*dx;
      for (j=0;2*j<nx;j++){
        fprintf(fout,"%e,%e,%e,%e\n",curX,curY,curZ,*pv);
        pv++;
        curX+=dx;
      }
      curY+=dy;
    }
    curZ+=dz;
  }
  fclose(fout);
}
/*void export1D(const char * name, double *val){
  FILE* fout=fopen(name,"w");
  int i;
  
  for (i=0;i<2*NGEN;i++){
    fprintf(fout,"%i  %e\n",i,*val++);
  }
  }*/
void initFftwTool(){
  outKernel = ( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * nx * ny*nzh );
  outEta = ( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * nx * ny*nzh );
  outRes = ( fftw_complex * ) fftw_malloc ( sizeof ( fftw_complex ) * nx * ny*nzh );

}
void resetFftwTool(){
  fftw_free(outKernel);
  fftw_free(outEta);
  fftw_free(outRes);
}
/*
 *Transformation de fourier du noyau afin de calculer les convolutions
 *
 */
void buildOutKernel(){
  int i;
  fftw_plan plan_forward;

  evalKernel();
  plan_forward = fftw_plan_dft_r2c_3d ( nx, ny, nz,inKernel, outKernel, FFTW_ESTIMATE );
  
  fftw_execute ( plan_forward );
  for (i=0;i<nx*ny*nzh;i++){
    outKernel[i][0]*=(dx*dy*dz)/(nx*ny*nz);
    outKernel[i][1]*=(dx*dy*dz)/(nx*ny*nz);
  }
  fftw_destroy_plan(plan_forward);
}
void checkSim(double *mat){
  int i,j,k;
   for (k=0;k<nz;k++)
    for (j=0;j<ny;j++)
      for (i=0;i<nx;i++)
        if (fabs(mat[i+j*nx+k*nx*ny]-mat[(nx-1-i)+(ny-1-j)*nx+(nz-1-k)*nx*ny])>1e-10)
          printf("k not sim %i %i %i %.12le %.12le\n",i,j,k,mat[i+j*nx+k*nx*ny],mat[(nx-1-i)+(ny-1-j)*nx+(nz-1-k)*nx*ny]);
}

void correctOffset(double *eta){
  int i,j,k;
  double *pD=eta+nx*ny*(nz-1);
  
  for (k=1;k<nz;k++){
    memcpy(pD,pD-nx*ny,nx*ny*sizeof(double));
    pD-=nx*ny;
  }
}
void zeroBordure(double *eta){
  for (int k=nz-10;k<nz;k++)
    for (int j=nx-10;j<nx;j++)
      for (int i=ny-10;i<ny;i++){
        eta[j+i*nx+k*nx*ny]=0;
      }
  for (int k=0;k<10;k++)
    for (int j=0;j<10;j++)
      for (int i=0;i<10;i++){
        eta[j+i*nx+k*nx*ny]=0;
      }
}

/*effectue le calul de la convolution
 *out=in*kernel
 *
 */
//,outKernel,outEta,outRes
void convol(double *in,double *out,double *buf){
  int i,j,k;
  fftw_plan plan_1;
  
  plan_1 = fftw_plan_dft_r2c_3d ( nx, ny, nz,in, outEta, FFTW_ESTIMATE );
  fftw_execute ( plan_1);
  fftw_destroy_plan(plan_1);
  for (i=0;i<nx *ny*nzh;i++){
    outRes[i][0]=(outEta[i][0]*outKernel[i][0]-outEta[i][1]*outKernel[i][1]);
    outRes[i][1]=(outEta[i][0]*outKernel[i][1]+outEta[i][1]*outKernel[i][0]);
  }
  fftw_plan plan_backward = fftw_plan_dft_c2r_3d ( nx, ny, nz,outRes, out, FFTW_ESTIMATE );

  fftw_execute ( plan_backward );
  fftw_destroy_plan(plan_backward);
  int nx2=nx/2;
  int ny2=ny/2;
  int nz2=nz/2;
  double * savout=out;
  //return;
  for (k=0;k<nz;k++){
    out=savout+nx*ny*k;
    for (j=0;j<ny2;j++){
      double *pLinHaut=out + (j*nx);
      double *pLinBas=out + ((j+ny2)*nx);
      size_t scp=nx2*sizeof(double);
      memcpy(buf,pLinBas+nx2,scp);
      memcpy(pLinBas+nx2,pLinHaut,scp);
      memcpy(pLinHaut,buf,scp);
      
      memcpy(buf,pLinHaut+nx2,scp);
      memcpy(pLinHaut+nx2,pLinBas,scp);
      memcpy(pLinBas,buf,scp);
    }
  }
  out=savout;
  for (k=0;k<nz2;k++){
    double *pplanHaut=out+k*nx*ny;
    double *pplanBas=out+(k+nz2)*nx*ny;
    size_t scp=nx*ny*sizeof(double);
    memcpy(buf,pplanHaut,scp);
    memcpy(pplanHaut,pplanBas,scp);
    memcpy(pplanBas,buf,scp);
                          
  }
  out=savout;
  //return;
  memcpy(buf,out,nx*ny*nz*sizeof(double));
//  correctOffset(out);
  for (k=1;k<nz;k++)
    for (i=1;i<ny;i++)
      for (j=1;j<nx;j++)
        out[j+i*nx+k*nx*ny]=0.5*buf[(j-1)+(i-1)*nx+(k-1)*nx*ny]+0.5*out[j+i*nx+k*nx*ny];

  zeroBordure(out);
}
