

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
//#include <iostream>
using namespace std;
#include <math.h>
#include <string.h>
#include "fftwtool.h"

enum CASETUDE {CAS1, CAS2, CAS3, CAS4, CAS5};

enum CASETUDE CurCas;

char OUTPUTDIR[512];
/*
 *definition du noyau centre en (0,0) en vue de la transformation de fourier
 *
 */
double lambda=5e-3;
void evalKernelswitched(){
  switch (CurCas)
  {
  case CAS1:
    break;
  case CAS2:
    break;
  case CAS3:
    break;
  default:
    printf("evalKernel, cas non prevu\n");
      
      
  }
}
void evalKernel(){
  int i,j,k;
  double xmiddle=(nx*dx)/2.0;
  double ymiddle=(ny*dy)/2.0;
  double zmiddle=(nz*dz)/2.0;
  double curX=0.5*dx;
  double curY=0.5*dy;
  double curZ=0.5*dz;
  
  double mass=0;
  for (k=0;k<nz;k++){
    curY=0.5*dy;
    for (j=0;j<ny;j++){
      curX=0.5*dx;
      for (i=0;i<nx;i++){
        double dd2=(curX-xmiddle)*(curX-xmiddle)+(curY-ymiddle)*(curY-ymiddle)+(curZ-zmiddle)*(curZ-zmiddle);
        inKernel[i+j*nx+k*nx*ny]=exp(-0.5*dd2/lambda);
        mass+=inKernel[i+j*nx+k*nx*ny]*dx*dy*dz;
        curX+=dx;
      }
      curY+=dy;
    }
    curZ+=dz;
  }
  double *pv=inKernel;
  for (i=0;i<nx*ny*nz;i++){
    *pv=(*pv)/mass;
    pv++;
  }
//  export3D("kernel.txt",inKernel);
  /*for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        inKernel[j+i*nx+k*nx*ny]=0;


  for (k=49;k<51;k++)
    for (i=49;i<51;i++)
      for (j=49;j<51;j++){
        inKernel[j+i*nx+k*nx*ny]=1;
        mass+=inKernel[i+j*nx+k*nx*ny]*dx*dy*dz;
      }
  double *pv=inKernel;
  for (i=0;i<nx*ny*nz;i++){
    *pv=(*pv)/mass;
    pv++;
    }*/
}

/*
 * Definition de l'etat initial
 *
 */
void initEtaSimple(double *eta){
  int i,j,k;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        eta[j+i*nx+k*nx*ny]=0;//(1.0*j)/(nx-1);

  for (k=0.45*nz;k<0.55*nz;k++)
    for (i=0.45*ny;i<0.55*ny;i++)
      for (j=0.45*nx;j<0.55*nx;j++)
      eta[j+i*nx+k*nx*ny]=1;

  /*for (k=0.2*nz;k<0.8*nz;k++)
    for (i=0.2*ny;i<0.8*ny;i++)
      for (j=0.2*nx;j<0.8*nx;j++)
      eta[j+i*nx+k*nx*ny]=1;*/

}
void initEtaU(double *eta){
  int i,j,k;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        eta[j+i*nx+k*nx*ny]=0;//(1.0*j)/(nx-1);

  for (k=0.3*nz;k<0.35*nz;k++)
    for (i=0.3*ny;i<0.4*ny;i++)
      for (j=0.3*nx;j<0.55*nx;j++)
      eta[j+i*nx+k*nx*ny]=1;

  for (k=0.3*nz;k<0.65*nz;k++)
    for (i=0.3*ny;i<0.4*ny;i++)
      for (j=0.3*nx;j<0.35*nx;j++)
        eta[j+i*nx+k*nx*ny]=1;

  for (k=0.3*nz;k<0.65*nz;k++)
    for (i=0.3*ny;i<0.4*ny;i++)
      for (j=0.5*nx;j<0.55*nx;j++)
        eta[j+i*nx+k*nx*ny]=1;


  /*for (k=0.2*nz;k<0.8*nz;k++)
    for (i=0.2*ny;i<0.8*ny;i++)
      for (j=0.2*nx;j<0.8*nx;j++)
      eta[j+i*nx+k*nx*ny]=1;*/

}
void printMaxLocation(double *eta){
  double max=-10;
  int i,j,k,imax,jmax,kmax;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        if (eta[j+i*nx+k*nx*ny]>max){
          imax=i;jmax=j;kmax=k;max=eta[j+i*nx+k*nx*ny];
        }
  printf("coord max: %i %i %i %le %le\n",imax,jmax,kmax, max,eta[17+23*nx+33*nx*ny]);
}
void printNonNulLocation(double *eta){
  double max=-10;
  int i,j,k,imax,jmax,kmax;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        if (eta[j+i*nx+k*nx*ny]!=0){
          printf("coord nn: %i %i %i %le\n",i,j,k, eta[j+i*nx+k*nx*ny]);
        }
}
void printAllMaxLocation(double *eta){
  double max=-1e200;
  int i,j,k,imax,jmax,kmax;
  printf("printAllMaxLocation ");
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        if (eta[j+i*nx+k*nx*ny]>max){
          imax=i;jmax=j;kmax=k;max=eta[j+i*nx+k*nx*ny];
        }
    for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        if (fabs(eta[j+i*nx+k*nx*ny]-max)<1e-10)
          printf("coord max: %i %i %i %13le\n",i,j,k, eta[j+i*nx+k*nx*ny]);
}
void cloche(double *eta){
  int i,j,k;
  double mass=0;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        eta[j+i*nx+k*nx*ny]=0;


  for (k=49;k<51;k++)
    for (i=49;i<51;i++)
      for (j=49;j<51;j++){
        eta[j+i*nx+k*nx*ny]=1;
        mass+=eta[j+i*nx+k*nx*ny]*(dx*dy*dz);
      }
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++){
        *eta/=mass;
        eta++;
      }
        


  
}
void initEta(double *eta){
//  initEtaU(eta);
  cloche(eta);
}
double computeMass(double *pv){
  int i;
  double res=0;
  for (i=0;i<nx*ny*nz;i++)
    res+=pv[i]*dx*dy*dz;
  return res;
}




/*
int main_test(){
  //spatial discretisation
  nx=2*2*50+1;
  ny=2*2*50+1;
  nz=2*2*50+1;
  int ncells=(nx/2)*(ny/2)*(nz/2);
  nzh = ( nz / 2 ) + 1;
  dx=0.01;
  dy=0.01;
  dz=0.01;
  initFftwTool();

  double * eta=(double *) calloc(nx * ny * nz,sizeof ( double ) ); 
  double * etac=(double *) malloc(sizeof ( double ) * nx * ny *nz);
  double * buf=(double *) malloc(sizeof ( double ) * nx * ny *nz);
  
  //le noyau
  int i,j,k;
  inKernel = ( double * ) malloc ( sizeof ( double ) * nx * ny *nz);
  buildOutKernel();
  printAllMaxLocation(inKernel);
  initEta(eta);
  printMaxLocation(eta);
  double massinit=computeMass(eta);
  saveVTK("etac0.vtk",eta);
  convol(eta,etac,buf);
  printMaxLocation(etac);
  double massconvol=computeMass(etac);printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  saveVTK("etac1.vtk",etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  massconvol=computeMass(eta);printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  saveVTK("etac2.vtk",eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  for (int g=0;g<100;g++){
    convol(eta,etac,buf);printMaxLocation(etac);
    convol(etac,eta,buf);printMaxLocation(eta);
  }
  massconvol=computeMass(etac);printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  saveVTK("etac3.vtk",etac);
  massconvol=computeMass(etac);
  printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  free(eta);
  free(etac);
  free(inKernel);
  resetFftwTool();
  }
*/
//#define DEBUG
double Tf=1;
double dt=0.05;
double lx,ly,lz;
double ox,oy,oz;

double *m=NULL;
double deltaCas1(double t){
  return 1.2e-3*t;
}
double deltaT(double t){
  return deltaCas1(t);
}

void setOptimPos(double t){
  ox=lx/2+deltaT(t);oy=ly/2;oz=lz/2;
}
void computem(double *m){
 
  int i,j,k;
  double curX=0.5*dx;
  double curY=0.5*dy;
  double curZ=0.5*dz;
  
  
  for (k=0;k<nz;k++){
    curY=0.5*dy;
    for (j=0;j<ny;j++){
      curX=0.5*dx;
      for (i=0;i<nx;i++){
          double dd2=(curX-ox)*(curX-ox)+(curY-oy)*(curY-oy)+(curZ-oz)*(curZ-oz);
          /*if (i==50 && j==50 && k==50){
            printf("curX=%le curY=%le curZ=%le \n",curX,curY,curZ);
            printf("ox=%le oy=%le oz=%le \n",ox,oy,oz);
            printf("dd2 middle =%le ii=%i\n",dd2,i+j*nx+k*nx*ny);
            }*/
          m[i+j*nx+k*nx*ny]=-0.5*dd2;
          
        curX+=dx;
      }
      curY+=dy;
    }
    curZ+=dz;
  }
  
  

}
double computembar(double *m,double *eta){
  double res=0;
  double poptot=0;
  for (int ii=0;ii<nx*ny*nz;ii++){
    /*if (eta[ii]!=0){
       printf("eta non nul %i\n",ii);// at i=%i j=%i k=%i\n",i,j,k);
       }*/
    res+=m[ii]*eta[ii]*dx*dy*dz;
    //   poptot+=eta[ii]*dx*dy*dz;
  }
  
  return res;///poptot;
}
void initDirac(double *eta){
  /*int i=nx/2;
  int j=ny/2;
  int k=nz/2;
  eta[i+j*nx+k*nx*ny]=1/(dx*dy*dz);*/


  int i,j,k;
  double mass=0;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        eta[j+i*nx+k*nx*ny]=0;
  if (nx%2==0){

    for (k=nz/2 -1;k<nz/2+1;k++)
      for (i=ny/2-1;i<ny/2+1;i++)
        for (j=nx/2-1;j<nx/2+1;j++){
          printf("%i %i %i\n",i,j,k);
          eta[j+i*nx+k*nx*ny]=1;
          mass+=eta[j+i*nx+k*nx*ny]*(dx*dy*dz);
        }
  }else{
    for (k=nz/2 -1;k<=nz/2+1;k++)
      for (i=ny/2-1;i<=ny/2+1;i++)
        for (j=nx/2-1;j<=nx/2+1;j++){
          printf("%i %i %i\n",i,j,k);
          eta[j+i*nx+k*nx*ny]=1;
          mass+=eta[j+i*nx+k*nx*ny]*(dx*dy*dz);
        }
    
  }
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++){
        *eta/=mass;
        eta++;
      }
        


  
}
FILE * openInfoFile(){
  char fileName[512];
  FILE *pInfo;
  switch (CurCas)
  {
  case CAS1:
    sprintf(OUTPUTDIR,"CAS1");
    sprintf(fileName,"%s/info.txt",OUTPUTDIR);
    
    break;
  case CAS2:
    sprintf(OUTPUTDIR,"CAS2");
    sprintf(fileName,"%s/info.txt",OUTPUTDIR);
    
    break;
  default:
    printf("openInfoFile: cas d'etude non prevu\n");
    return NULL;
  }
  pInfo=fopen(fileName,"a");
  return pInfo;
}

void writeInfo(){
  char fileName[512];
  FILE *pInfo;
  switch (CurCas)
  {
  case CAS1:
    sprintf(OUTPUTDIR,"CAS1");
    sprintf(fileName,"%s/info.txt",OUTPUTDIR);
    
    break;
  case CAS2:
    sprintf(OUTPUTDIR,"CAS2");
    sprintf(fileName,"%s/info.txt",OUTPUTDIR);
    
    break;
  default:
    printf("cas d'etude non prevu\n");
    return;
  }
  pInfo=fopen(fileName,"w");
  fprintf(pInfo,"lx %e\nly %e\nlz %e\n",lx,ly,lz);
  fprintf(pInfo,"nx %i\nny %i\nnz %i\n",nx,ny,nz);

switch (CurCas)
  {
  case CAS1:
    break;
  case CAS2:
    break;
  default:
    printf("cas d'etude non prevu\n");
    return;
  }


  
  fclose(pInfo);
}

int main(){
  char filename[512];
  CurCas=CAS1;
  
  lx=5;
  ly=3;
  lz=3;
  //spatial discretisation
  nx=2*2*50;
  ny=2*50;
  nz=2*50;
  m=(double*)malloc(nx*ny*nz*sizeof(double));
  int nxnynz=nx*ny*nz;
//  int ncells=(nx/2)*(ny/2)*(nz/2);
  nzh = ( nz / 2 ) + 1;
  dx=lx/nx;
  dy=ly/ny;
  dz=lz/nz;
  initFftwTool();
  writeInfo();

  double * eta=(double *) calloc(nx * ny * nz,sizeof ( double ) ); 
  double * etaprev=(double *) calloc(nx * ny * nz,sizeof ( double ) ); 
  double * etac=(double *) malloc(sizeof ( double ) * nx * ny *nz);
  double * buf=(double *) malloc(sizeof ( double ) * nx * ny *nz);
  
  //le noyau
  int i,j,k;
  inKernel = ( double * ) malloc ( sizeof ( double ) * nx * ny *nz);
  buildOutKernel();
  //checkKsim();
  sprintf(filename,"%s/kernel.vtk",OUTPUTDIR);
  saveVTK(filename,inKernel);
  //printAllMaxLocation(inKernel);
  initDirac(eta);
  //cloche(eta);
  double massinit=computeMass(eta);
  sprintf(filename,"%s/etat0.vtk",OUTPUTDIR);
  saveVTK(filename,eta);
  //printMaxLocation(eta);
  //printNonNulLocation(eta);
  double curT=0;
  setOptimPos(curT);
  computem(m);
  double mbar=computembar(m,eta);
  sprintf(filename,"%s/mbar.txt",OUTPUTDIR);
  FILE *pF=fopen(filename,"w");
  fprintf(pF,"%le %le\n",curT,mbar);
  fclose(pF);
  printf("mbar init %le\n",mbar);
  printAllMaxLocation(eta);
  Tf=1000;
  dt=0.1;
  int NSteps=Tf/dt;
  double U=0.1125;
  {
    FILE *pF=openInfoFile();
    fprintf(pF,"Tf %le\ndt %le\nNSteps %i\nU %le\n",Tf,dt,NSteps,U);
    fclose(pF);
  }
  char name[256];
  for (int numStep=1;numStep<NSteps;numStep++){
    printf("doing step %i on %i\n",numStep,NSteps);
    //memcpy(etaprev,eta,sizeof ( double ) * nx * ny *nz);
    double curT=numStep*dt;
    setOptimPos(curT);
    computem(m);
    //printAllMaxLocation(eta);
    convol(eta,etac,buf);
    
    //printAllMaxLocation(etac);
    for (int ii=0;ii<nx*ny*nz;ii++){
      etac[ii]-=eta[ii];
      etac[ii]*=U;
      etac[ii]+=eta[ii]*(m[ii]-mbar);
      eta[ii]+=dt*etac[ii];
      
    }
    double masscur=computeMass(eta);
    for (int ii=0;ii<nx*ny*nz;ii++)
      eta[ii]/=masscur;

    printf("masse %le %le\n",massinit,masscur);
    if (numStep%10 == 0 || mbar < -1000){
      sprintf(name,"%s/eta%i.vtk",OUTPUTDIR,numStep);
      saveVTK(name,eta);
    }
    //if (mbar < -1e6)
    //  break;
    
    
    //printAllMaxLocation(eta);
    //printAllMaxLocation(eta);
    //break;
    
    mbar=computembar(m,eta);
    {
      sprintf(filename,"%s/mbar.txt",OUTPUTDIR);
      pF=fopen(filename,"a");
      fprintf(pF,"%le %le\n",curT,mbar);
      fclose(pF);
    }
    printf(" %le\n",mbar);
    //dt u=U(J*u-u)+u(m-mbar)
    // u_tp1=u_t+dt*(U(J*u_t-u_t)+u(m_tp1-mbar_t))
    // U=0.1125
    //output mbar
    
  }
//  fclose(pF);
  //printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  free(eta);
  free(m);
  free(etac);
  free(inKernel);
  resetFftwTool();
}
