

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
//#include <iostream>
using namespace std;
#include <math.h>
#include <string.h>
#include "fftwtool.h"
#include <unistd.h>


char OUTPUTDIR[512];
/*
 *definition du noyau centre en (0,0) en vue de la transformation de fourier
 *
 */
int UCASE;
int CCASE;
int PRECISION;
 
double n=3;
double lambda=5e-3;
double Uc=0;
double mu=0;
double U=0.1125;
double C=0;
double offsetX;

int withVTK=0;
int nxBeforeOri,nyBeforeOri,nzBeforeOri;
int scenario=1;

void evalKernel(){
  int i,j,k;
  double xmiddle=(nx*dx)/2.0;
  double ymiddle=(ny*dy)/2.0;
  double zmiddle=(nz*dz)/2.0;
  double curX=0.5*dx;
  double curY=0.5*dy;
  double curZ=0.5*dz;
  
  double mass=0;
  for (k=0;k<nz;k++){
    curY=0.5*dy;
    for (j=0;j<ny;j++){
      curX=0.5*dx;
      for (i=0;i<nx;i++){
        double dd2=(curX-xmiddle)*(curX-xmiddle)+(curY-ymiddle)*(curY-ymiddle)+(curZ-zmiddle)*(curZ-zmiddle);
        inKernel[i+j*nx+k*nx*ny]=exp(-0.5*dd2/lambda);
        mass+=inKernel[i+j*nx+k*nx*ny]*dx*dy*dz;
        curX+=dx;
      }
      curY+=dy;
    }
    curZ+=dz;
  }
  double *pv=inKernel;
  for (i=0;i<nx*ny*nz;i++){
    *pv=(*pv)/mass;
    pv++;
  }
//  export3D("kernel.txt",inKernel);
  /*for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        inKernel[j+i*nx+k*nx*ny]=0;


  for (k=49;k<51;k++)
    for (i=49;i<51;i++)
      for (j=49;j<51;j++){
        inKernel[j+i*nx+k*nx*ny]=1;
        mass+=inKernel[i+j*nx+k*nx*ny]*dx*dy*dz;
      }
  double *pv=inKernel;
  for (i=0;i<nx*ny*nz;i++){
    *pv=(*pv)/mass;
    pv++;
    }*/
}

/*
 * Definition de l'etat initial
 *
 */
void initEtaSimple(double *eta){
  int i,j,k;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        eta[j+i*nx+k*nx*ny]=0;//(1.0*j)/(nx-1);

  for (k=0.45*nz;k<0.55*nz;k++)
    for (i=0.45*ny;i<0.55*ny;i++)
      for (j=0.45*nx;j<0.55*nx;j++)
      eta[j+i*nx+k*nx*ny]=1;

  /*for (k=0.2*nz;k<0.8*nz;k++)
    for (i=0.2*ny;i<0.8*ny;i++)
      for (j=0.2*nx;j<0.8*nx;j++)
      eta[j+i*nx+k*nx*ny]=1;*/

}
void initEtaU(double *eta){
  int i,j,k;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        eta[j+i*nx+k*nx*ny]=0;//(1.0*j)/(nx-1);

  for (k=0.3*nz;k<0.35*nz;k++)
    for (i=0.3*ny;i<0.4*ny;i++)
      for (j=0.3*nx;j<0.55*nx;j++)
      eta[j+i*nx+k*nx*ny]=1;

  for (k=0.3*nz;k<0.65*nz;k++)
    for (i=0.3*ny;i<0.4*ny;i++)
      for (j=0.3*nx;j<0.35*nx;j++)
        eta[j+i*nx+k*nx*ny]=1;

  for (k=0.3*nz;k<0.65*nz;k++)
    for (i=0.3*ny;i<0.4*ny;i++)
      for (j=0.5*nx;j<0.55*nx;j++)
        eta[j+i*nx+k*nx*ny]=1;


  /*for (k=0.2*nz;k<0.8*nz;k++)
    for (i=0.2*ny;i<0.8*ny;i++)
      for (j=0.2*nx;j<0.8*nx;j++)
      eta[j+i*nx+k*nx*ny]=1;*/

}
void printMaxLocation(double *eta){
  double max=-10;
  int i,j,k,imax,jmax,kmax;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        if (eta[j+i*nx+k*nx*ny]>max){
          imax=i;jmax=j;kmax=k;max=eta[j+i*nx+k*nx*ny];
        }
  printf("coord max: %i %i %i %le %le\n",imax,jmax,kmax, max,eta[17+23*nx+33*nx*ny]);
}
void printNonNulLocation(double *eta){
  double max=-10;
  int i,j,k,imax,jmax,kmax;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        if (eta[j+i*nx+k*nx*ny]!=0){
          printf("coord nn: %i %i %i %le\n",i,j,k, eta[j+i*nx+k*nx*ny]);
        }
}
void printAllMaxLocation(double *eta){
  double max=-1e200;
  int i,j,k,imax,jmax,kmax;
  printf("printAllMaxLocation ");
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        if (eta[j+i*nx+k*nx*ny]>max){
          imax=i;jmax=j;kmax=k;max=eta[j+i*nx+k*nx*ny];
        }
    for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        if (fabs(eta[j+i*nx+k*nx*ny]-max)<1e-10)
          printf("coord max: %i %i %i %13le\n",i,j,k, eta[j+i*nx+k*nx*ny]);
}
void cloche(double *eta){
  int i,j,k;
  double mass=0;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        eta[j+i*nx+k*nx*ny]=0;


  for (k=49;k<51;k++)
    for (i=49;i<51;i++)
      for (j=49;j<51;j++){
        eta[j+i*nx+k*nx*ny]=1;
        mass+=eta[j+i*nx+k*nx*ny]*(dx*dy*dz);
      }
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++){
        *eta/=mass;
        eta++;
      }
        


  
}
void initEta(double *eta){
//  initEtaU(eta);
  cloche(eta);
}
double computeMass(double *pv){
  int i;
  double res=0;
  for (i=0;i<nx*ny*nz;i++)
    res+=pv[i]*dx*dy*dz;
  return res;
}




/*
int main_test(){
  //spatial discretisation
  nx=2*2*50+1;
  ny=2*2*50+1;
  nz=2*2*50+1;
  int ncells=(nx/2)*(ny/2)*(nz/2);
  nzh = ( nz / 2 ) + 1;
  dx=0.01;
  dy=0.01;
  dz=0.01;
  initFftwTool();

  double * eta=(double *) calloc(nx * ny * nz,sizeof ( double ) ); 
  double * etac=(double *) malloc(sizeof ( double ) * nx * ny *nz);
  double * buf=(double *) malloc(sizeof ( double ) * nx * ny *nz);
  
  //le noyau
  int i,j,k;
  inKernel = ( double * ) malloc ( sizeof ( double ) * nx * ny *nz);
  buildOutKernel();
  printAllMaxLocation(inKernel);
  initEta(eta);
  printMaxLocation(eta);
  double massinit=computeMass(eta);
  saveVTK("etac0.vtk",eta);
  convol(eta,etac,buf);
  printMaxLocation(etac);
  double massconvol=computeMass(etac);printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  saveVTK("etac1.vtk",etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  massconvol=computeMass(eta);printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  saveVTK("etac2.vtk",eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  convol(eta,etac,buf);printMaxLocation(etac);
  convol(etac,eta,buf);printMaxLocation(eta);
  for (int g=0;g<100;g++){
    convol(eta,etac,buf);printMaxLocation(etac);
    convol(etac,eta,buf);printMaxLocation(eta);
  }
  massconvol=computeMass(etac);printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  saveVTK("etac3.vtk",etac);
  massconvol=computeMass(etac);
  printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  free(eta);
  free(etac);
  free(inKernel);
  resetFftwTool();
  }
*/
//#define DEBUG
double Tf=1;
double dt=0.05;
double lx,ly,lz;
double ox,oy,oz;
double oxOri,oyOri,ozOri;
double DeltaMax=100;

double *m=NULL;
double deltaCas1(double t){
  return C*t;
}
double deltaSc2(double t){
  double aux=sin(M_PI*C*t);
  return sqrt(lambda)*DeltaMax*aux*aux;
}
double deltaSc3(double t){
  return sqrt(lambda)*DeltaMax*(exp(-C*t));
}
double deltaSc4(double t){
  return sqrt(lambda)*DeltaMax*(1-exp(-C*t));

}
double deltaT(double t){
  if (scenario==1)
    return deltaCas1(t);
  if (scenario==2)
    return deltaSc2(t);
  if (scenario==3)
    return deltaSc3(t);
  else
    return deltaSc4(t);
}

void setOptimPos(double t){
  ox=2+deltaT(t);
  oy=ly/2;
  oz=lz/2;
}
void computem(double *m){
 
  int i,j,k;
  double curX=0.5*dx;
  double curY=0.5*dy;
  double curZ=0.5*dz;
  
  
  for (k=0;k<nz;k++){
    curY=0.5*dy;
    for (j=0;j<ny;j++){
      curX=0.5*dx+offsetX;
      for (i=0;i<nx;i++){
          double dd2=(curX-ox)*(curX-ox)+(curY-oy)*(curY-oy)+(curZ-oz)*(curZ-oz);
          /*if (i==50 && j==50 && k==50){
            printf("curX=%le curY=%le curZ=%le \n",curX,curY,curZ);
            printf("ox=%le oy=%le oz=%le \n",ox,oy,oz);
            printf("dd2 middle =%le ii=%i\n",dd2,i+j*nx+k*nx*ny);
            }*/
          m[i+j*nx+k*nx*ny]=-0.5*dd2;
          
        curX+=dx;
      }
      curY+=dy;
    }
    curZ+=dz;
  }
  
  

}
double computembar(double *m,double *eta){
  double res=0;
  double poptot=0;
  for (int ii=0;ii<nx*ny*nz;ii++){
    /*if (eta[ii]!=0){
       printf("eta non nul %i\n",ii);// at i=%i j=%i k=%i\n",i,j,k);
       }*/
    res+=m[ii]*eta[ii]*dx*dy*dz;
    //   poptot+=eta[ii]*dx*dy*dz;
  }
  
  return res;///poptot;
}
double computexbar(double * eta){
  double res=0;
  int i,j,k;
  double curX=0.5*dx;
  double curY=0.5*dy;
  double curZ=0.5*dz;
  
  
  for (k=0;k<nz;k++){
    curY=0.5*dy;
    for (j=0;j<ny;j++){
      curX=0.5*dx;
      for (i=0;i<nx;i++){
          res+=curX*dx*dy*dz*eta[i+j*nx+k*nx*ny];
        curX+=dx;
      }
      curY+=dy;
    }
    curZ+=dz;
  }
  return res;
}
/*
 *if (nOffsetX>0) on avance le signal
 *  eta(ix+nOffsetX)=eta(ix)
 *else on recule le signale
 *  eta(ix+nOffsetX)=eta(ix)
 *
 */
void offsetEta(int nOffsetX,double * eta){
int k,j,i;
  if (nOffsetX>0){
    double lostMass=0;
    for (k=0;k<nz;k++){
      for (j=0;j<ny;j++){
        for (i=nx-nOffsetX;i<nx;i++){
          lostMass=eta[i+j*nx+k*nx*ny];
          
        }
      }
    }
    printf("xbar mass lost=%le\n",lostMass);


    
    for (k=0;k<nz;k++){
      for (j=0;j<ny;j++){
        for (i=0;i<nx-nOffsetX;i++){
          eta[i+nOffsetX+j*nx+k*nx*ny]=eta[i+j*nx+k*nx*ny];
          
        }
      }
    }

    for (k=0;k<nz;k++){
      for (j=0;j<ny;j++){
        for (i=0;i<nOffsetX;i++){
          eta[i+j*nx+k*nx*ny]=0;
          
        }
      }
    }
    
  }else{
    /*double lostMass=0;
    int nMissCase=0;
    for (k=0;k<nz;k++){
      for (j=0;j<ny;j++){
        for (i=0;i<-nOffsetX;i++){
          lostMass=eta[i+j*nx+k*nx*ny];
          nMissCase++;
        }
      }
    }
    printf("xbar mass lost=%le\n",lostMass);
    printf("xbar miss case=%i\n",nMissCase);
    */
    for (k=0;k<nz;k++){
      for (j=0;j<ny;j++){
        for (i=-nOffsetX;i<nx;i++){
          eta[i+nOffsetX+j*nx+k*nx*ny]=eta[i+j*nx+k*nx*ny];
        }
      }
    }

    for (k=0;k<nz;k++){
      for (j=0;j<ny;j++){
        for (i=nx+nOffsetX;i<nx;i++){
          eta[i+j*nx+k*nx*ny]=0;
        }
      }
    }
  }
}

void initDirac(double *eta){
  /*int i=nx/2;
  int j=ny/2;
  int k=nz/2;
  eta[i+j*nx+k*nx*ny]=1/(dx*dy*dz);*/


  int i,j,k;
  double mass=0;
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++)
        eta[j+i*nx+k*nx*ny]=0;
  if (nx%2==0){

    for (k=nzBeforeOri -1;k<nzBeforeOri+1;k++)
      for (i=nyBeforeOri-1;i<nyBeforeOri+1;i++)
        for (j=nxBeforeOri-1;j<nxBeforeOri+1;j++){
          printf("%i %i %i\n",i,j,k);
          eta[j+i*nx+k*nx*ny]=1;
          mass+=eta[j+i*nx+k*nx*ny]*(dx*dy*dz);
        }
  }else{
    
    for (k=nzBeforeOri-1;k<=nzBeforeOri+1;k++)
      for (j=nyBeforeOri-1;j<=nyBeforeOri+1;j++)
        for (i=nxBeforeOri-1;i<=nxBeforeOri+1;i++){
          printf("%i %i %i\n",i,j,k);
          eta[i+j*nx+k*nx*ny]=1;
          mass+=eta[i+j*nx+k*nx*ny]*(dx*dy*dz);
        }
    
  }
  
  for (k=0;k<nz;k++)
    for (i=0;i<ny;i++)
      for (j=0;j<nx;j++){
        *eta/=mass;
        eta++;
        //eta[j+i*nx+k*nx*ny]=inKernel[i+j*nx+k*nx*ny];
      }
        

  
}
FILE * openInfoFile(){
  char fileName[512];
  FILE *pInfo;
  sprintf(OUTPUTDIR,"CAS%i_%i/",CCASE,UCASE);
  sprintf(fileName,"%s/info%i.txt",OUTPUTDIR,PRECISION);
  pInfo=fopen(fileName,"a");
  return pInfo;
}

void writeInfo(){
  char fileName[512];
  FILE *pInfo;
  sprintf(OUTPUTDIR,"CAS%i_%i/",CCASE,UCASE);
  sprintf(fileName,"%s/info%i.txt",OUTPUTDIR,PRECISION);
  pInfo=fopen(fileName,"w");
  fprintf(pInfo,"UCASE=%i\nCCASE=%i\nPRECISION=%i\n",UCASE,CCASE,PRECISION);
  fprintf(pInfo,"lx=%e\nly=%e\nlz=%e\n",lx,ly,lz);
  fprintf(pInfo,"nx=%i\nny=%i\nnz=%i\n",nx,ny,nz);
  fprintf(pInfo,"n=%le\nlambda=%le\nUc=%le\nU=%le\nmu=%le\nC=%le\n",n,lambda,Uc,U,mu,C);

  fclose(pInfo);
}

int main(int argc,char *argv[]){
  //position reelle = position +offsetX
  offsetX=0;
  Uc=(n*n*lambda)/4.0;
  
  if (argc != 4){
    printf("usage: simulator: ccase(1-3 and 4 for scenario2 5 for sce3 6 for sce 4) ucase(1-4) precision(1-2)\n");
    return 0;
  }
  CCASE=atoi(argv[1]);
  UCASE=atoi(argv[2]);
  PRECISION=atoi(argv[3]);
  printf("doing U %i C %i\n",UCASE,CCASE);
  
  if (UCASE==1)
    U=Uc/10.0;
  else if (UCASE==2)
    U=Uc;
  else if (UCASE==3)
    U=10*Uc;
  else if (UCASE==4)
    U=100*Uc;
  else
    printf("UCASE > 4\n");
  mu=sqrt(lambda*U);          
          
  if (CCASE==1)
    C=0.05*mu;
  else if (CCASE==2)
    C=0.5*mu;
  else if (CCASE==3)
    C=5*mu;
  else if (CCASE==4){
    scenario=2;
    C=0.5*mu;
  }else if (CCASE==5){
    scenario=3;
    C=0.5*mu;
  }else if (CCASE==6){
    scenario=4;
    C=0.5*mu;
  }else if (CCASE==7){
    C=sqrt(mu*mu*mu*n);
  }else if (CCASE==8){
    C=sqrt(0.5*mu*mu*mu*n);
  }else
    printf("CCASE > 8\n");
  
      
  char filename[512];
  
  
  lx=5;
  ly=5;
  lz=5;
  oxOri=2;
  oyOri=0.5*ly;
  ozOri=0.5*lz;
  int sx,sy,sz;
  //spatial discretisation
  if (PRECISION==1){
    nx=4*2*2*50;sx=6;
    ny=4*2*2*50;sy=6;
    nz=4*2*2*50;sz=6;

/*    nx=4*2*50;sx=3;
    ny=4*2*50;sy=3;
    nz=4*2*50;sz=3;*/

  }else{
    nx=2*3*2*2*50;sx=2*6;
    ny=2*3*2*2*50;sy=2*6;
    nz=2*3*2*2*50;sz=2*6;
  }
  nxBeforeOri=round((nx*oxOri)/lx);
  nyBeforeOri=round((ny*oyOri)/ly);
  nzBeforeOri=round((nz*ozOri)/lz);
  printf("%i %i %i\n",nxBeforeOri,nyBeforeOri,nzBeforeOri);
  m=(double*)malloc(nx*ny*nz*sizeof(double));
  int nxnynz=nx*ny*nz;
//  int ncells=(nx/2)*(ny/2)*(nz/2);
  nzh = ( nz / 2 ) + 1;
  dx=lx/nx;
  dy=ly/ny;
  dz=lz/nz;
  initFftwTool();
  writeInfo();

  double * eta=(double *) calloc(nx * ny * nz,sizeof ( double ) ); 
  double * etaprev=(double *) calloc(nx * ny * nz,sizeof ( double ) ); 
  double * etac=(double *) malloc(sizeof ( double ) * nx * ny *nz);
  double * buf=(double *) malloc(sizeof ( double ) * nx * ny *nz);
  
  //le noyau
  int i,j,k;
  inKernel = ( double * ) malloc ( sizeof ( double ) * nx * ny *nz);
  buildOutKernel();
  //checkKsim();
  if (withVTK){
    sprintf(filename,"%s/kernel%i.vtk",OUTPUTDIR,PRECISION);
    saveVTKStep(filename,inKernel,sx,sy,sz);
  }
  //printAllMaxLocation(inKernel);
  initDirac(eta);
  //cloche(eta);
  double massinit=computeMass(eta);
  if (withVTK){
    sprintf(filename,"%s/eta%is0.vtk",OUTPUTDIR,PRECISION);
    saveVTKStep(filename,eta,sx,sy,sz);
  }
  //printMaxLocation(eta);
  //printNonNulLocation(eta);
  double curT=0;
  setOptimPos(curT);
  computem(m);
  double mbar=computembar(m,eta);
  sprintf(filename,"%s/mbar%i.txt",OUTPUTDIR,PRECISION);
  FILE *pF=fopen(filename,"w");
  fprintf(pF,"%le %le\n",curT,mbar);
  fclose(pF);
  printf("mbar init %le\n",mbar);
  printAllMaxLocation(eta);
  Tf=0.2*10.0/mu;
  if (CCASE==7 || CCASE==8){
    if (UCASE==1)
      Tf=3000;
    if (UCASE==2)
      Tf=1000;
    if (UCASE==3)
      Tf=300;
        
  }
  
  dt=Tf/100.0;
  int NSteps=100;
  
  {
    char hostbuffer[256];
    gethostname(hostbuffer, sizeof(hostbuffer)); 
    FILE *pF=openInfoFile();
    fprintf(pF,"Hostname: %s\n", hostbuffer); 
    fprintf(pF,"Tf=%le\ndt=%le\nNSteps=%i\n",Tf,dt,NSteps);
    if (scenario>1)
      fprintf(pF,"DeltaMax=%le\n",DeltaMax);
    fclose(pF);
  }
  char name[256];
  for (int numStep=1;numStep<NSteps;numStep++){
    printf("doing step %i on %i\n",numStep,NSteps);
    //memcpy(etaprev,eta,sizeof ( double ) * nx * ny *nz);
    double curT=numStep*dt;
    setOptimPos(curT);
    computem(m);
    //printAllMaxLocation(eta);
    convol(eta,etac,buf);
    
    //printAllMaxLocation(etac);
    for (int ii=0;ii<nx*ny*nz;ii++){
      etac[ii]-=eta[ii];
      etac[ii]*=U;
      etac[ii]+=eta[ii]*(m[ii]-mbar);
      eta[ii]+=dt*etac[ii];
      
      if (eta[ii]<0){
        
        eta[ii]=0;
      }
      //eta[ii]=etac[ii];
      
    }
    //printAllMaxLocation(eta);
    double masscur=computeMass(eta);
    for (int ii=0;ii<nx*ny*nz;ii++)
      eta[ii]/=masscur;

    printf("masse %le %le\n",massinit,masscur);
   
    if (withVTK ){
      sprintf(name,"%s/eta%is%i.vtk",OUTPUTDIR,PRECISION,numStep);
      saveVTKStep(name,eta,sx,sy,sz);
    }
    //if (mbar < -1e6)
    //  break;
    
    
    //printAllMaxLocation(eta);
    //printAllMaxLocation(eta);
    //break;
    
    mbar=computembar(m,eta);
    printf("mbar %le\n",mbar);
    double xbar=computexbar(eta);
    printf("xbar=%le\n",xbar);
    if(fabs(xbar-lx/2)>1){
      int nOffsetX=fabs(xbar-lx/2)/dx;
      if (xbar-lx/2 >0)
        nOffsetX=-nOffsetX;
      printf("nOffset=%i\n",nOffsetX);
	    offsetEta(nOffsetX,eta);
      offsetX+=-nOffsetX*dx;
      xbar=computexbar(eta);
      printf("xbar offseted=%le\n",xbar);
      computem(m);
      mbar=computembar(m,eta);
      printf("mbar offseted %le\n",mbar);
    }
    masscur=computeMass(eta);
    for (int ii=0;ii<nx*ny*nz;ii++)
      eta[ii]/=masscur;
    xbar=computexbar(eta);
    printf("xbar offseted2=%le\n",xbar);
    
    {
      sprintf(filename,"%s/mbar%i.txt",OUTPUTDIR,PRECISION);
      pF=fopen(filename,"a");
      fprintf(pF,"%le %le\n",curT,mbar);
      fclose(pF);
    }
    printf(" %le\n",mbar);
    //dt u=U(J*u-u)+u(m-mbar)
    // u_tp1=u_t+dt*(U(J*u_t-u_t)+u(m_tp1-mbar_t))
    // U=0.1125
    //output mbar
    
  }
//  fclose(pF);
  //printf("massinit=%e massconvol=%e\n",massinit,massconvol);
  free(eta);
  free(m);
  free(etac);
  free(inKernel);
  resetFftwTool();
}
