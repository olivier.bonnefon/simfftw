#ifndef FFTWTOOL_H
#define FFTWTOOL_H

extern int nx;
extern int ny;
extern int nz;
extern int nzh;
extern double dx;
extern double dy;
extern double dz;
extern double *inKernel;


void initFftwTool();
void resetFftwTool();

//fonction defined by user
void evalKernel();
void saveVTKStep(const char * name, double *val,int sx,int sy,int sz);
void saveVTK(const char * name, double *val);
void export3D(const char * name, double *val);
void buildOutKernel();
void convol(double *in,double *out,double *buf);
#endif
