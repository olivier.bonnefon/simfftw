

# include <stdlib.h>
# include <stdio.h>
# include <time.h>

# include <fftw3.h>
# include <math.h>
#include <string.h>

int nx;
int ny;
int nyh;
double dx;
double dy;

double *inKernel;

fftw_complex *outKernel;


void export3D(const char * name, double *val){
  FILE* fout=fopen(name,"w");
  int i,j;
  double curX=0.5*dx;
  double curY=0.5*dy;
  
  double *pv=val;
  for (j=0;j<ny;j++){
    curX=0.5*dx;
    for (i=0;i<nx;i++){
      fprintf(fout,"%e %e %e\n",curX,curY,*pv);
      pv++;
      curX+=dx;
    }
    curY+=dy;
  }
  fclose(fout);
}


void evalKernel(){
  int i,j;
  double xmiddle=(nx*dx)/2.0;
  double ymiddle=(ny*dy)/2.0;
  double curX=0.5*dx;
  double curY=0.5*dy;
  
  double mass=0;
  
  for (j=0;j<ny;j++){
    curX=0.5*dx;
    for (i=0;i<nx;i++){
      double dd2=(curX-xmiddle)*(curX-xmiddle)+(curY-ymiddle)*(curY-ymiddle);
      inKernel[i+j*nx]=exp(-7*dd2);
      mass+=inKernel[i+j*nx]*dx*dy;
      curX+=dx;
    }
    curY+=dy;
  }
  double *pv=inKernel;
  for (i=0;i<nx*ny;i++){
    *pv=(*pv)/mass;
    pv++;
  }
  export3D("kernel.txt",inKernel);
}

void buildOutKernel(){
  int i;
  fftw_plan plan_forward;
  evalKernel();
  plan_forward = fftw_plan_dft_r2c_2d ( nx, ny, inKernel, outKernel, FFTW_ESTIMATE );
  
  fftw_execute ( plan_forward );
  for (i=0;i<nx*nyh;i++){
    outKernel[i][0]*=(dx*dy)/(nx*ny);
    outKernel[i][1]*=(dx*dy)/(nx*ny);
  }
  
}

void initEta(double *eta){
  int i,j;
  for (j=0;j<ny;j++)
    for (i=0;i<nx;i++)
      eta[i+j*nx]=0;
  
  for (j=10;j<17;j++)
    for (i=5;i<10;i++)
      eta[i+j*nx]=1;
  
  for (j=70;j<80;j++)
    for (i=55;i<60;i++)
      eta[i+j*nx]=1;
}

double computeMass(double *pv){
  int i;
  double res=0;
  for (i=0;i<nx*ny;i++)
    res+=pv[i]*dx*dy;
  return res;
}
void convol(double *in,double *out,double *buf,fftw_complex *kernel,fftw_complex *buffft1,fftw_complex *buffft2){
  int i,j;
  fftw_plan plan_1;
  plan_1 = fftw_plan_dft_r2c_2d ( nx, ny, in, buffft1, FFTW_ESTIMATE );
  fftw_execute ( plan_1);
  for (i=0;i<nx * nyh;i++){
    buffft2[i][0]=(buffft1[i][0]*kernel[i][0]-buffft1[i][1]*kernel[i][1]);
    buffft2[i][1]=(buffft1[i][0]*kernel[i][1]+buffft1[i][1]*kernel[i][0]);
  }
  fftw_plan plan_backward = fftw_plan_dft_c2r_2d ( nx, ny, buffft2, out, FFTW_ESTIMATE );

  fftw_execute ( plan_backward );
  
  int nx2=nx/2;
  int ny2=ny/2;

  for (j=0;j<ny2;j++){
    double *pLinHaut=out + (j*nx);
    double *pLinBas=out + ((j+nx2)*nx);
    size_t scp=nx2*sizeof(double);
    memcpy(buf,pLinBas+nx2,scp);
    memcpy(pLinBas+nx2,pLinHaut,scp);
    memcpy(pLinHaut,buf,scp);

    memcpy(buf,pLinHaut+nx2,scp);
    memcpy(pLinHaut+nx2,pLinBas,scp);
    memcpy(pLinBas,buf,scp);
  }
}
int main(){
  nx=100;
  ny=100;
  nyh = ( ny / 2 ) + 1;
  dx=0.1;
  dy=0.1;
  nyh = ( ny / 2 ) + 1;
  double * eta0=(double *) malloc(sizeof ( double ) * nx * ny );
  double * eta0c=(double *) malloc(sizeof ( double ) * nx * ny );
  double * buf=(double *) malloc(sizeof ( double ) * (nx+ny) );
  int i,j;
  inKernel = ( double * ) malloc ( sizeof ( double ) * nx * ny );
  
  outKernel = fftw_malloc ( sizeof ( fftw_complex ) * nx * nyh );
  fftw_complex *outEta = fftw_malloc ( sizeof ( fftw_complex ) * nx * nyh );
  fftw_complex *outRes = fftw_malloc ( sizeof ( fftw_complex ) * nx * nyh );
  buildOutKernel();

  initEta(eta0);
  export3D("eta0",eta0);
  printf("mass init=%e\n",computeMass(eta0));
  fftw_plan plan_1;
  plan_1 = fftw_plan_dft_r2c_2d ( nx, ny, eta0, outEta, FFTW_ESTIMATE );
  fftw_execute ( plan_1);
  for (i=0;i<nx * nyh;i++){
    outRes[i][0]=(outEta[i][0]*outKernel[i][0]-outEta[i][1]*outKernel[i][1]);
    outRes[i][1]=(outEta[i][0]*outKernel[i][1]+outEta[i][1]*outKernel[i][0]);
  }
  fftw_plan plan_backward = fftw_plan_dft_c2r_2d ( nx, ny, outRes, eta0c, FFTW_ESTIMATE );

  fftw_execute ( plan_backward );
  int nx2=nx/2;
  int ny2=ny/2;

  for (j=0;j<ny2;j++){
    double *pLinHaut=eta0c + (j*nx);
    double *pLinBas=eta0c + ((j+nx2)*nx);
    size_t scp=nx2*sizeof(double);
    memcpy(buf,pLinBas+nx2,scp);
    memcpy(pLinBas+nx2,pLinHaut,scp);
    memcpy(pLinHaut,buf,scp);

    memcpy(buf,pLinHaut+nx2,scp);
    memcpy(pLinHaut+nx2,pLinBas,scp);
    memcpy(pLinBas,buf,scp);
  }
  
/*  for (i=0;i<nx;i++){
    double * pLin=eta0c + (i*nx);
    for (j=nx2;j<nx;j++)
      buf[j-nx2]=pLin[j];
    for (j=0;j<nx2;j++)
      pLin[nx-nx2+j]=pLin[j];
    for (j=nx2;j<nx;j++)
      pLin[j-nx2]=buf[j-nx2];
  }
  for (j=0;j<ny;j++){
    for (i=ny2;i<ny;i++)
      buf[i-ny2]=eta0c[i*nx+j];
    for (i=0;i<ny2;i++)
      eta0c[(ny-ny2+i)*nx+j]=eta0c[i*nx+j];
    for (i=ny2;i<ny;i++)
      eta0c[j+(i-ny2)*nx]=buf[j-ny2];
      }*/
  
//for (i=0;i<nx * ny;i++)
  //  eta0c[i]=eta0c[i]/2500;
  export3D("eta0c",eta0c);
  printf("mass covolée=%e\n",computeMass(eta0c));
}
